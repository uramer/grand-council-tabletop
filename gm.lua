local DEBUG = false

local vec2
do
    local vec2Meta = {
        __add = function(a, b)
            return vec2(a.x + b.x, a.y + b.y)
        end,
        __mul = function(a, b)
            return vec2(a.x * b.x, a.y * b.y)
        end,
        __tostring = function(self)
            return ('(%d, %d)'):format(self.x, self.y)
        end,
    }
    vec2 = function(x, y)
        y = y or x
        return setmetatable({
            x = x,
            y = y,
        }, vec2Meta)
    end
end

local function gm(args)
    local cmd = 'gm ' .. table.concat(args, ' ')
    if DEBUG then print(cmd) end
    local stream = io.popen(cmd, 'r')
    local result = stream:read('*a')
    stream:close()
    return result
end

local command
do
    local write = function(self, outputPath)
        self:add(('"%s"'):format(outputPath))
        gm(self.__args)
    end
    local add = function(self, str)
        table.insert(self.__args, str)
        return self
    end
    local path = function(self, path)
        return self:add('"' .. path .. '"')
    end
    local font = function(self, family, size)
        return self
            :add('-font "' .. family .. '"')
            :add('-pointsize ' .. tostring(size))
    end
    local text = function(self, text, pos)
        pos = pos or vec2(0)
        text = text
            :gsub('\'', '\\\'')
            :gsub('\r', '')
            :gsub('\n', '\\n')
        return self:add(('-draw "text %d,%d \'%s\'"'):format(pos.x, pos.y, text))
    end
    local fill = function(self, ...)
        local col = { ... }
        for i = 1, 4 do
            col[i] = (col[i] or 0) * 65535
        end
        return self:add(('-fill "rgba(%d,%d,%d,%d)"'):format(unpack(col)))
    end
    command = function(cmd)
        return {
            __args = { cmd },
            write = write,
            add = add,
            path = path,
            font = font,
            text = text,
            fill = fill,
        }
    end
end

local function getSize(src)
    local result = gm {
        'identify',
        '-format "%w %h"',
        src,
    }
    local size = {}
    for substring in result:gmatch("%S+") do
        table.insert(size, tonumber(substring))
    end
    return vec2(size[1], size[2])
end

return {
    DEBUG = DEBUG,
    vec2 = vec2,
    command = command,
    getSize = getSize,
}