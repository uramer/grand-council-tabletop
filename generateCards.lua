local gm = require('gm')
local DEBUG, vec2, command, getSize = gm.DEBUG, gm.vec2, gm.command, gm.getSize

local FONT = 'Arial'

local function nameHeight(baseSize)
    return baseSize.y * 0.1
end

local function mechanicsHeight(baseSize)
    return baseSize.y * 0.2
end

local function artwork(cmd, src, baseSize)
    local topOffset = nameHeight(baseSize)
    local bottomOffset = mechanicsHeight(baseSize)
    cmd:add(('-resize x%d'):format(baseSize.y - topOffset - bottomOffset))
        :add('-gravity North')
        :add(('-geometry +%d+%d'):format(0, topOffset))
        :add(src)
end

local function name(cmd, name, baseSize)
    local height = nameHeight(baseSize)
    local fontSize = height * 0.7
    local offset = vec2(0, fontSize)
    cmd:font(FONT, fontSize)
        :add('-gravity North')
        :text(name, offset)
end

local statOrder = { 'Seduction', 'Honor', 'Treachery', 'Faith', 'Magicka' }
    local function stats(cmd, stats, baseSize)
        local fontSize = baseSize.y * 0.05
        cmd:font(FONT, fontSize)
        cmd:add('-gravity SouthWest')
        local topOffset = vec2(0, mechanicsHeight(baseSize) + fontSize)

        local count = 0
        for i = #statOrder, 1, -1 do -- invert the order because of Souch gravity
            local statName = statOrder[i]
            local value = stats[statName] or 0
            if value > 0 then
                local label = ('%s: %d'):format(statName, value)
                local offset = topOffset + baseSize * vec2(0.05) * vec2(1, count)
                cmd:text(label, offset)
                count = count + 1
            end
        end
    end

local function mechanics(cmd, mechanics, baseSize)
    local height = mechanicsHeight(baseSize)
    local lines = 1
    for i = 1, #mechanics do
        if mechanics[i] == '\n' then
            lines = lines + 1
        end
    end
    local fontSize = math.min(math.floor(height / lines), baseSize.y * 0.04)
    cmd:font(FONT, fontSize)
    cmd:add('-gravity South')
    cmd:text(mechanics, vec2(0, fontSize))
end

local councilor = function(councilor, basePath)
    local baseSize = getSize(basePath)
    local tmpPath = councilor.name .. '.tmp.bmp'

    local base = command('composite')
    artwork(base, councilor.artwork, baseSize)
    base:path(basePath)
    base:write(tmpPath)

    local card = command('convert')
        :path(tmpPath)
        :fill(1, 1, 1)
    name(card, councilor.name, baseSize)
    stats(card, councilor.stats, baseSize)
    mechanics(card, councilor.mechanics, baseSize)

    local outPath = 'councilors/' .. councilor.name .. '.png'
    card:write(outPath)

    if not DEBUG then
        os.remove(tmpPath)
    end
end

local action = function(action, basePath)
    local baseSize = getSize(basePath)
    local tmpPath = action.name .. '.tmp.bmp'

    local base = command('composite')
    artwork(base, action.artwork, baseSize)
    base:path(basePath)
    base:write(tmpPath)

    local card = command('convert')
        :path(tmpPath)
        :fill(1, 1, 1)
    name(card, action.name, baseSize)
    stats(card, action.stats, baseSize)
    mechanics(card, action.mechanics, baseSize)

    local outPath = 'actions/' .. action.name .. '.png'
    card:write(outPath)

    if not DEBUG then
        os.remove(tmpPath)
    end
end

local basePath = 'assets/base.bmp'

local councilors = require('data.councilors')
for _, c in pairs(councilors) do
    councilor(c, basePath)
end

local actions = require('data.actions')
for _, a in pairs(actions) do
    action(a, basePath)
end
