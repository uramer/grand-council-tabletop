return {
  duel = {
    name = 'Duel',
    artwork = 'assets/duel.jpg',
    stats = {
        Honor = 3,
    },
    mechanics =
[[Discard a Counselor and
replace them with your
retainer with higher Honor]],
  }
}