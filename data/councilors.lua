return {
    aryon = {
        name = "Aryon",
        artwork = 'assets/aryon.jpg',
        stats = {
            Magicka = 2,
            Seduction = 2,
        },
        mechanics =
[[Total Magicka > 4: 5 VP
Stop the Mages' Guild monopoly.]],
    },
    fyr = {
        name = 'Divayth Fyr',
        artwork = 'assets/fyr.jpg',
        stats = {
            Magicka = 3,
            Honor = 1,
        },
        mechanics =
[[Total Faith > 3: 3 VP
All Corprus victims will now
be sent the Corprusarium.]],
    },
}