local gm = require('gm')
local command = gm.command

local function mosaic(directory)
    command('montage')
        :path(directory .. '/*.png')
        :add('-geometry +0+0')
        :write('mosaics/' .. directory .. '.png')
end

mosaic('councilors')
mosaic('actions')
