(let*
  (
    (width 1000)
    (height 1600)
    (image (car (gimp-image-new width height RGB)))
  )
  (
    (gimp-image-add-layer
      (car (gimp-layer-new image width height RGB-IMAGE "layer 1" 100 LAYER-MODE-NORMAL))
      0
    )
    (gimp-display-new image)
  ) 
)